import { createRef, createContext, Dispatch } from "react";
import { store, Actions } from "./store";

const appRef = createRef<HTMLDivElement>();

export type TAppContext = {
  store: AppState;
  dispatch: Dispatch<Actions>;
  actions: typeof store.actions;
};

const AppContext = createContext<TAppContext | null>(null);

export { appRef, AppContext, store };
