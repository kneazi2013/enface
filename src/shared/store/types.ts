export type ChangeAction = { type: "change"; payload: number };
export type ClearAction = { type: "clear" };
export type BackspaceAction = { type: "backspace" };
export type Actions = ChangeAction | ClearAction | BackspaceAction;