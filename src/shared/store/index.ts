import { actionCollection } from "./actions";
import { ActionType } from "./constants";
import type { ChangeAction, ClearAction, BackspaceAction } from "./types";
import { Actions } from "./types";

const defState: AppState = {
  password: "",
};

const reducer = (...args: [AppState, Actions]): AppState => {
  const [, action] = args;

  // @ts-ignore
  return actionCollection[action.type]?.(...args) || state;
};

const actions = {
  change: (payload: number): ChangeAction => ({
    type: ActionType.Change,
    payload,
  }),
  clear: (): ClearAction => ({
    type: ActionType.Clear,
  }),
  backspace: (): BackspaceAction => ({
    type: ActionType.Backspace,
  }),
};

export const store = {
  reducer,
  defState,
  actions,
};

export type { Actions };
