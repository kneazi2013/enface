import { ActionType } from "./constants";
import type { ChangeAction, ClearAction, BackspaceAction } from "./types";

const change = (...args: [AppState, ChangeAction]) => {
  const [state, { payload }] = args;
  if (10 <= state.password.length) return state;
  return { ...state, password: `${state.password}${payload}` };
};

const clear = (...args: [AppState, ClearAction]) => {
  const [state] = args;
  return { ...state, password: "" };
};

const backspace = (...args: [AppState, BackspaceAction]) => {
  const [state] = args;
  return { ...state, password: state.password.slice(0, -1) };
};

export const actionCollection = {
  [ActionType.Change]: change,
  [ActionType.Clear]: clear,
  [ActionType.Backspace]: backspace
}


