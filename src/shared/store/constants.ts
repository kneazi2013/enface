export enum ActionType {
  Change = "change",
  Clear = "clear",
  Backspace = "backspace",
}
