import React, { useReducer } from "react";
import { appRef, AppContext } from "shared";
import { ViewValue, InputPanel } from "components";
import { store } from "shared";
import * as AppStyle from "./app.style";

type Props = typeof store;

const App = (props: Props) => {
  const { reducer, defState, actions } = props;
  const [store, dispatch] = useReducer(reducer, defState);

  return (
    <AppStyle.Root ref={appRef}>
      <AppContext.Provider value={{ dispatch, store, actions }}>
        <AppStyle.Title>Confirm your passcode</AppStyle.Title>
        <AppStyle.View>
          <ViewValue />
        </AppStyle.View>
        <InputPanel />
      </AppContext.Provider>
    </AppStyle.Root>
  );
};

App.defaultProps = { ...store };

export default App;
