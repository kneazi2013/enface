import React, { useContext } from "react";
import { AppContext } from "shared";
import { ButtonProps } from "components";

type HandleClickEvent = React.MouseEvent<HTMLButtonElement, MouseEvent>;

const compose = (...funcs: Function[]) =>
  funcs.reduce((a, b) => (...args: any) => a(b(...args)));

const preventDefault = (event: HandleClickEvent) => event.preventDefault();

export const useGetCollection = (): ButtonProps[] => {
  const context = useContext(AppContext);
  if (!context) return [];
  const { dispatch, actions } = context;
  const { change } = actions;

  return [
    {
      caption: "1",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 1), preventDefault)(event),
    },
    {
      caption: "2",
      desc: "abc",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 2), preventDefault)(event),
    },
    {
      caption: "3",
      desc: "def",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 3), preventDefault)(event),
    },
    {
      caption: "4",
      desc: "jkl",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 4), preventDefault)(event),
    },
    {
      caption: "5",
      desc: "mno",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 5), preventDefault)(event),
    },
    {
      caption: "6",
      desc: "pqr",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 6), preventDefault)(event),
    },
    {
      caption: "7",
      desc: "stu",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 7), preventDefault)(event),
    },
    {
      caption: "8",
      desc: "vwx",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 8), preventDefault)(event),
    },
    {
      caption: "9",
      desc: "yz",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, change.bind(null, 9), preventDefault)(event),
    },
    {
      caption: "",
      desc: "Clear",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, actions.clear, preventDefault)(event),
    },
    {
      caption: "0",
      desc: "",
      onClick: (event: HandleClickEvent) =>
        compose(
          dispatch,
          actions.change.bind(null, 0),
          preventDefault
        )(event),
    },
    {
      caption: "",
      desc: "Delete",
      onClick: (event: HandleClickEvent) =>
        compose(dispatch, actions.backspace, preventDefault)(event),
    },
  ];
};
