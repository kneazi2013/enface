import React from "react";
import { Button } from "components";
import { useGetCollection } from "./hooks";
import * as Components from "./input-panel.style";

export const InputPanel = () => {
  const collection = useGetCollection();

  const content = collection.map((item, index) => {
    const { caption, desc, onClick } = item;
    return (
      <Components.Li key={index}>
        <Button caption={caption} desc={desc} onClick={onClick} />
      </Components.Li>
    );
  });

  return <Components.Ul>{content}</Components.Ul>;
};
