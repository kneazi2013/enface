import styled from "styled-components";

export const Ul = styled.ul`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
`;

export const Li = styled.li`
  display: flex;
  align-items: center;
  justify-content: center;
  list-style: none;
  padding: 1.75vw;
`;
