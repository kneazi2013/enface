import styled from "styled-components";

export const Button = styled.button`
  width: 20vmin;
  min-width: 75px;
  height: 20vmin;
  min-height: 75px;
  border: 1px solid #bbbbbb;
  background-color: #ffffff;
  border-radius: 5vmin;
  outline: none !important;
  padding: 0;

  &:hover {
    cursor: pointer;
  }
`;

export const Caption = styled.span`
  display: block;
  font-size: 3.2rem;
`;

export const Description = styled.span`
  display: block;
  font-size: 1rem;
  color: #bbbbbb;
`;
