import React from "react";
import * as Components from "./button.style";

export type ButtonProps = {
  onClick: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  caption: string;
  desc?: string;
};

export const Button = (props: ButtonProps) => {
  const { onClick, caption, desc } = props;

  return (
    <Components.Button onClick={onClick}>
      <Components.Caption>{caption}</Components.Caption>
      <Components.Description>{desc}</Components.Description>
    </Components.Button>
  );
};
