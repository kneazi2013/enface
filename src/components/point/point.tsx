import React from "react";
import * as PointComponent from "./point.style";
import type { Props } from "./types";

const Point = (props: Props) => (
  <PointComponent.Cell>
    <PointComponent.Icon
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 16 16"
      {...props}
    >
      <circle cx="8" cy="8" r="8" />
      <path d="M8,0a8,8,0,1,0,8,8A8,8,0,0,0,8,0ZM8,14.5A6.5,6.5,0,1,1,14.5,8,6.51,6.51,0,0,1,8,14.5Z" />
    </PointComponent.Icon>
  </PointComponent.Cell>
);

Point.defaultProps = {
  isActive: false,
};

export { Point };
