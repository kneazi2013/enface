import styled from "styled-components";
import type { Props } from "./types";

export const Cell = styled.i`
  display: block;
  padding: 1vmin;
`;

export const Icon = styled.svg<Props>`
  display: block;
  width: 4vmin;
  height: 4vmin;

  > path {
    fill: #bbbbbb;
  }

  > circle {
    fill: ${({ isActive }: Props) => (isActive ? "#bbbbbb" : "#ffffff")};
  }
`;
