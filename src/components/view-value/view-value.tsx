import React, { useContext, useMemo } from "react";
import { AppContext } from "shared";
import { Point } from "components/point";
import * as ViewValueComponent from "./view-value.style";

export const ViewValue = () => {
  const context = useContext(AppContext);
  const password = context?.store?.password || "";

  const content = useMemo(() => {
    const passwordArraySymbol = password.split("");

    return [...new Array(10)].map((_, index) => {
      const isActive = !!passwordArraySymbol[index];
      return <Point key={index} isActive={isActive} />;
    });
  }, [password]);

  return <ViewValueComponent.Container>{content}</ViewValueComponent.Container>;
};
