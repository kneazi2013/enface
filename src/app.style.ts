import styled from "styled-components";

export const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 32px;
`;

export const Title = styled.h1`
  display: block;
  font-size: 6vmin;
  margin-bottom: 10vmin;
`;

export const View = styled.div`
  margin-bottom: 5vmin;
`;